/// <reference types="react" />
interface IPart {
    id: number;
    name: string;
}
export default function ({ partData, onCb, ...params }: {
    partData: IPart[];
    onCb: any;
}): JSX.Element;
export {};
