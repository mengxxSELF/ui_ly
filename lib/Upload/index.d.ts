import React from 'react';
export interface IUploadFile {
    maxSize?: number;
    chunkSize?: number;
    onCb: (data: any) => void;
    url: string;
}
declare const _default: React.NamedExoticComponent<IUploadFile>;
export default _default;
