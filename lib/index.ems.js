import 'antd/dist/antd.css';
import React, { memo, useState } from 'react';
import { Card, Space, Avatar, Row, Col, Divider, Tag, Form, Upload, Button, Popconfirm, Statistic, Modal, Input, Select, Typography, Table as Table$1 } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { connect, Provider } from 'react-redux';
import { createStore } from 'redux';

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

const { Meta } = Card;
const UserCard = (_a) => {
    var _b;
    var { id } = _a, defaultProps = __rest(_a, ["id"]);
    const { user } = defaultProps || { user: null };
    return (React.createElement(Card, Object.assign({ title: `访客 ${id} 信息卡`, style: { width: 300 }, hoverable: true }, defaultProps),
        React.createElement(Space, { style: { width: '100%' }, size: 20, direction: "vertical" },
            user ? (React.createElement(Meta, { avatar: React.createElement(Avatar, { src: user === null || user === void 0 ? void 0 : user.avatar }), title: (user === null || user === void 0 ? void 0 : user.name) || '暂无用户名', description: (user === null || user === void 0 ? void 0 : user.description) || '暂无描述信息' })) : null,
            React.createElement(Row, { gutter: [10, 10] },
                React.createElement(Col, { span: 10 }, "\u54A8\u8BE2\u6B21\u6570"),
                React.createElement(Col, null, (user === null || user === void 0 ? void 0 : user.visitorCount) || 0)),
            React.createElement(Row, { gutter: [10, 10] },
                React.createElement(Col, { span: 10 }, "VIP"),
                React.createElement(Col, null, (user === null || user === void 0 ? void 0 : user.isVip) ? '是' : '否')),
            React.createElement(Row, { gutter: [10, 10] },
                React.createElement(Col, { span: 10 }, "\u610F\u5411\u5206\u6790"),
                React.createElement(Col, null, (_b = user === null || user === void 0 ? void 0 : user.purpose) === null || _b === void 0 ? void 0 : _b.join(',')))),
        React.createElement(Divider, null),
        React.createElement(Space, null, (user === null || user === void 0 ? void 0 : user.tags) && user.tags.map((item) => React.createElement(Tag, { color: "cyan" }, item)))));
};
var index$4 = memo(UserCard);

function upload({ file, chunkSize: SIZE, onCb, url }) {
    // 切片编号
    let num = 0;
    // 文件大小
    const size = file.size;
    const filename = file.name;
    // 切片数量
    const count = Math.ceil(size / SIZE);
    // 唯一key 每一个文件的key都不同
    const key = Date.now();
    // 开启上传
    while (num < count) {
        const min = Math.min(SIZE, size - num * SIZE);
        const [start, end] = [num * SIZE, num * SIZE + min];
        // 本次需要上传的内容
        const uploadContent = file.slice(start, end);
        uploadData({
            data: uploadContent,
            filename,
            count,
            num,
            key,
            url,
            type: filename.split('.').pop(),
        }).then((data) => onCb(data));
        num++;
    }
}
function uploadData({ data, filename, count, num, key, url, type }) {
    return new Promise((resolve) => {
        // 二进制传输
        const formD = new FormData();
        formD.append('filename', filename);
        formD.append('chunk', data);
        formD.append('key', key);
        // 分片编号
        formD.append('chunkname', num);
        formD.append('totalCount', count);
        const xhr = new XMLHttpRequest();
        xhr.open('post', `${url}?type=${type}&filename=${encodeURI(filename)}`);
        xhr.onload = function () {
            if (this.status == 200) {
                resolve(this.response);
            }
        };
        xhr.send(formD);
    });
}

const normFile = (e) => {
    if (Array.isArray(e)) {
        return e;
    }
    return e && e.fileList;
};
const UploadFile = (props) => {
    const [form] = Form.useForm();
    const { maxSize, onCb, chunkSize = 0.2, url } = props;
    const SIZE = 1024 * 1024 * chunkSize; // 每片大小
    const onUpload = () => {
        const { dragger } = form.getFieldsValue();
        const [{ originFileObj }] = dragger;
        upload({
            file: originFileObj,
            chunkSize: SIZE,
            onCb,
            url,
        });
    };
    return (React.createElement(Form, { name: "control-ref" },
        React.createElement(Form.Item, { name: "dragger", valuePropName: "fileList", getValueFromEvent: normFile, noStyle: true },
            React.createElement(Upload.Dragger, { name: "files" },
                React.createElement("p", { className: "ant-upload-drag-icon" },
                    React.createElement(InboxOutlined, null)),
                React.createElement("p", { className: "ant-upload-hint" },
                    "\u4E0A\u4F20\u6587\u4EF6\u4E0D\u5F97\u8D85\u8FC7 ",
                    maxSize || 100,
                    " MB"))),
        React.createElement(Form.Item, { style: { marginTop: 20 } },
            React.createElement(Button, { type: "primary", onClick: onUpload }, "\u786E\u5B9A\u4E0A\u4F20"))));
};
var index$3 = memo(UploadFile);

const { Countdown } = Statistic;
const DeviceCountdown = (props) => {
    const { id, time, cb } = props, defaultProps = __rest(props, ["id", "time", "cb"]);
    return (React.createElement(Card, { title: `设备 ${id} 有效期倒计时` },
        React.createElement(Countdown, Object.assign({ value: time }, defaultProps)),
        React.createElement(Popconfirm, { style: { marginTop: 20 }, title: `确认对设备 ${id} 执行续签操作`, onConfirm: cb, okText: "Yes", cancelText: "No" },
            React.createElement(Button, null, " \u7EED\u7B7E "))));
};
var index$2 = memo(DeviceCountdown);

const { Option } = Select;
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
function index$1 (_a) {
    var { partData, onCb } = _a, params = __rest(_a, ["partData", "onCb"]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [form] = Form.useForm();
    const handleOk = () => {
        const { name, part } = form.getFieldsValue();
        onCb({
            data: { name, part },
            cb: () => setIsModalVisible(false),
        });
    };
    const defaultModal = Object.assign({ width: 600, title: '创建子区设备块' }, params);
    return (React.createElement(React.Fragment, null,
        React.createElement(Button, { type: "primary", onClick: () => setIsModalVisible(true) }, "\u521B\u5EFA\u5B50\u533A\u8BBE\u5907\u5757"),
        React.createElement(Modal, Object.assign({ visible: isModalVisible, onOk: handleOk, onCancel: () => setIsModalVisible(false) }, defaultModal),
            React.createElement(Form, Object.assign({}, layout, { form: form }),
                React.createElement(Form.Item, { name: "name", label: "\u540D\u79F0", rules: [{ required: true }] },
                    React.createElement(Input, null)),
                React.createElement(Form.Item, { name: "part", label: "\u53EF\u9009\u533A", rules: [{ required: true }] },
                    React.createElement(Select, { allowClear: true }, partData.map((part) => {
                        const { id, name } = part;
                        return (React.createElement(Option, { key: id, value: name }, name));
                    })))))));
}

const defaultstate = {
    checked: [],
};
const tableReducer = (state = defaultstate, action) => {
    switch (action.type) {
        case 'check':
            return Object.assign(Object.assign({}, state), { checked: action.data });
        default:
            return state;
    }
};

const store = createStore(tableReducer);

const { Text } = Typography;
const Index$1 = ({ checked = [], overdue, link, }) => {
    return (React.createElement(Space, null,
        React.createElement(Text, { type: "success" },
            " \u5DF2\u52FE\u9009\u8BBE\u5907 ",
            checked.join('、')),
        React.createElement(Button, { onClick: () => overdue(checked), type: "default", danger: true }, "\u8FC7\u671F"),
        React.createElement(Button, { onClick: () => link(checked), type: "primary" }, "\u91CD\u8FDE")));
};
var Actions = connect(({ checked }) => {
    return {
        checked: checked,
    };
})(Index$1);

const columns = [
    {
        title: 'id',
        dataIndex: 'id',
    },
    {
        title: '名称',
        dataIndex: 'name',
    },
];
const Index = ({ data, onSelect }) => {
    const selectTable = (ids) => {
        onSelect(ids);
    };
    return (React.createElement(Table$1, { rowKey: "id", pagination: false, dataSource: data, columns: columns, rowSelection: { onChange: selectTable } }));
};
var Table = connect(null, (dispatch) => {
    return {
        onSelect: (ids) => {
            dispatch({
                type: 'check',
                data: ids,
            });
        },
    };
})(Index);

var index = ({ id, data, overdue, link }) => {
    return (React.createElement(Provider, { store: store },
        React.createElement(Space, { style: { width: '100%' }, direction: "vertical" },
            React.createElement(Row, { justify: "space-between" },
                React.createElement(Col, null,
                    React.createElement("p", null,
                        " \u8BBE\u5907\u5757 ",
                        id,
                        " \u5F53\u524D\u8FDE\u63A5\u677F ")),
                React.createElement(Col, null,
                    React.createElement(Actions, Object.assign({}, { overdue, link })))),
            React.createElement(Table, { data: data }))));
};

export { index$1 as Device, index$2 as DeviceCountdown, index as Link, index$3 as Upload, index$4 as Visitor };
