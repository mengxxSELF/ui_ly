/// <reference types="react" />
declare const _default: import("react-redux").ConnectedComponent<({ checked, overdue, link, }: {
    checked: number[];
    overdue: (ids: number[]) => void;
    link: (ids: number[]) => void;
}) => JSX.Element, Omit<{
    checked: number[];
    overdue: (ids: number[]) => void;
    link: (ids: number[]) => void;
}, "checked"> & import("react-redux").ConnectProps>;
export default _default;
