/// <reference types="react" />
declare const _default: import("react-redux").ConnectedComponent<({ data, onSelect }: {
    data: any[];
    onSelect: any;
}) => JSX.Element, Omit<{
    data: any[];
    onSelect: any;
}, "onSelect"> & import("react-redux").ConnectProps>;
export default _default;
