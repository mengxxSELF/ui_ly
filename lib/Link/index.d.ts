/// <reference types="react" />
export interface ILink {
    id: number;
    data: any[];
    overdue: (ids: number[]) => void;
    link: (ids: number[]) => void;
}
declare const _default: ({ id, data, overdue, link }: ILink) => JSX.Element;
export default _default;
