import React from 'react';
export interface ICountdown {
    id: number;
    time: number;
    cb: () => void;
    [key: string]: any;
}
declare const _default: React.NamedExoticComponent<ICountdown>;
export default _default;
