import React from 'react';
export interface IUserAttr {
    avatar: string;
    name: string;
    visitorCount: number;
    isVip: boolean;
    tags: string[];
    purpose: string[];
    description: string;
}
export interface IUserCard {
    id: number;
    user?: Partial<IUserAttr>;
}
declare const _default: React.NamedExoticComponent<IUserCard>;
export default _default;
