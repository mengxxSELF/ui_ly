(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('antd/dist/antd.css'), require('react'), require('antd'), require('@ant-design/icons'), require('react-redux'), require('redux')) :
    typeof define === 'function' && define.amd ? define(['exports', 'antd/dist/antd.css', 'react', 'antd', '@ant-design/icons', 'react-redux', 'redux'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.ui_ly = {}, null, global.React, global.antd, global.icons, global.reactRedux, global.redux));
})(this, (function (exports, antd_css, React, antd, icons, reactRedux, redux) { 'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var React__default = /*#__PURE__*/_interopDefaultLegacy(React);

    /******************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    const { Meta } = antd.Card;
    const UserCard = (_a) => {
        var _b;
        var { id } = _a, defaultProps = __rest(_a, ["id"]);
        const { user } = defaultProps || { user: null };
        return (React__default["default"].createElement(antd.Card, Object.assign({ title: `访客 ${id} 信息卡`, style: { width: 300 }, hoverable: true }, defaultProps),
            React__default["default"].createElement(antd.Space, { style: { width: '100%' }, size: 20, direction: "vertical" },
                user ? (React__default["default"].createElement(Meta, { avatar: React__default["default"].createElement(antd.Avatar, { src: user === null || user === void 0 ? void 0 : user.avatar }), title: (user === null || user === void 0 ? void 0 : user.name) || '暂无用户名', description: (user === null || user === void 0 ? void 0 : user.description) || '暂无描述信息' })) : null,
                React__default["default"].createElement(antd.Row, { gutter: [10, 10] },
                    React__default["default"].createElement(antd.Col, { span: 10 }, "\u54A8\u8BE2\u6B21\u6570"),
                    React__default["default"].createElement(antd.Col, null, (user === null || user === void 0 ? void 0 : user.visitorCount) || 0)),
                React__default["default"].createElement(antd.Row, { gutter: [10, 10] },
                    React__default["default"].createElement(antd.Col, { span: 10 }, "VIP"),
                    React__default["default"].createElement(antd.Col, null, (user === null || user === void 0 ? void 0 : user.isVip) ? '是' : '否')),
                React__default["default"].createElement(antd.Row, { gutter: [10, 10] },
                    React__default["default"].createElement(antd.Col, { span: 10 }, "\u610F\u5411\u5206\u6790"),
                    React__default["default"].createElement(antd.Col, null, (_b = user === null || user === void 0 ? void 0 : user.purpose) === null || _b === void 0 ? void 0 : _b.join(',')))),
            React__default["default"].createElement(antd.Divider, null),
            React__default["default"].createElement(antd.Space, null, (user === null || user === void 0 ? void 0 : user.tags) && user.tags.map((item) => React__default["default"].createElement(antd.Tag, { color: "cyan" }, item)))));
    };
    var index$4 = React.memo(UserCard);

    function upload({ file, chunkSize: SIZE, onCb, url }) {
        // 切片编号
        let num = 0;
        // 文件大小
        const size = file.size;
        const filename = file.name;
        // 切片数量
        const count = Math.ceil(size / SIZE);
        // 唯一key 每一个文件的key都不同
        const key = Date.now();
        // 开启上传
        while (num < count) {
            const min = Math.min(SIZE, size - num * SIZE);
            const [start, end] = [num * SIZE, num * SIZE + min];
            // 本次需要上传的内容
            const uploadContent = file.slice(start, end);
            uploadData({
                data: uploadContent,
                filename,
                count,
                num,
                key,
                url,
                type: filename.split('.').pop(),
            }).then((data) => onCb(data));
            num++;
        }
    }
    function uploadData({ data, filename, count, num, key, url, type }) {
        return new Promise((resolve) => {
            // 二进制传输
            const formD = new FormData();
            formD.append('filename', filename);
            formD.append('chunk', data);
            formD.append('key', key);
            // 分片编号
            formD.append('chunkname', num);
            formD.append('totalCount', count);
            const xhr = new XMLHttpRequest();
            xhr.open('post', `${url}?type=${type}&filename=${encodeURI(filename)}`);
            xhr.onload = function () {
                if (this.status == 200) {
                    resolve(this.response);
                }
            };
            xhr.send(formD);
        });
    }

    const normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };
    const UploadFile = (props) => {
        const [form] = antd.Form.useForm();
        const { maxSize, onCb, chunkSize = 0.2, url } = props;
        const SIZE = 1024 * 1024 * chunkSize; // 每片大小
        const onUpload = () => {
            const { dragger } = form.getFieldsValue();
            const [{ originFileObj }] = dragger;
            upload({
                file: originFileObj,
                chunkSize: SIZE,
                onCb,
                url,
            });
        };
        return (React__default["default"].createElement(antd.Form, { name: "control-ref" },
            React__default["default"].createElement(antd.Form.Item, { name: "dragger", valuePropName: "fileList", getValueFromEvent: normFile, noStyle: true },
                React__default["default"].createElement(antd.Upload.Dragger, { name: "files" },
                    React__default["default"].createElement("p", { className: "ant-upload-drag-icon" },
                        React__default["default"].createElement(icons.InboxOutlined, null)),
                    React__default["default"].createElement("p", { className: "ant-upload-hint" },
                        "\u4E0A\u4F20\u6587\u4EF6\u4E0D\u5F97\u8D85\u8FC7 ",
                        maxSize || 100,
                        " MB"))),
            React__default["default"].createElement(antd.Form.Item, { style: { marginTop: 20 } },
                React__default["default"].createElement(antd.Button, { type: "primary", onClick: onUpload }, "\u786E\u5B9A\u4E0A\u4F20"))));
    };
    var index$3 = React.memo(UploadFile);

    const { Countdown } = antd.Statistic;
    const DeviceCountdown = (props) => {
        const { id, time, cb } = props, defaultProps = __rest(props, ["id", "time", "cb"]);
        return (React__default["default"].createElement(antd.Card, { title: `设备 ${id} 有效期倒计时` },
            React__default["default"].createElement(Countdown, Object.assign({ value: time }, defaultProps)),
            React__default["default"].createElement(antd.Popconfirm, { style: { marginTop: 20 }, title: `确认对设备 ${id} 执行续签操作`, onConfirm: cb, okText: "Yes", cancelText: "No" },
                React__default["default"].createElement(antd.Button, null, " \u7EED\u7B7E "))));
    };
    var index$2 = React.memo(DeviceCountdown);

    const { Option } = antd.Select;
    const layout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    };
    function index$1 (_a) {
        var { partData, onCb } = _a, params = __rest(_a, ["partData", "onCb"]);
        const [isModalVisible, setIsModalVisible] = React.useState(false);
        const [form] = antd.Form.useForm();
        const handleOk = () => {
            const { name, part } = form.getFieldsValue();
            onCb({
                data: { name, part },
                cb: () => setIsModalVisible(false),
            });
        };
        const defaultModal = Object.assign({ width: 600, title: '创建子区设备块' }, params);
        return (React__default["default"].createElement(React__default["default"].Fragment, null,
            React__default["default"].createElement(antd.Button, { type: "primary", onClick: () => setIsModalVisible(true) }, "\u521B\u5EFA\u5B50\u533A\u8BBE\u5907\u5757"),
            React__default["default"].createElement(antd.Modal, Object.assign({ visible: isModalVisible, onOk: handleOk, onCancel: () => setIsModalVisible(false) }, defaultModal),
                React__default["default"].createElement(antd.Form, Object.assign({}, layout, { form: form }),
                    React__default["default"].createElement(antd.Form.Item, { name: "name", label: "\u540D\u79F0", rules: [{ required: true }] },
                        React__default["default"].createElement(antd.Input, null)),
                    React__default["default"].createElement(antd.Form.Item, { name: "part", label: "\u53EF\u9009\u533A", rules: [{ required: true }] },
                        React__default["default"].createElement(antd.Select, { allowClear: true }, partData.map((part) => {
                            const { id, name } = part;
                            return (React__default["default"].createElement(Option, { key: id, value: name }, name));
                        })))))));
    }

    const defaultstate = {
        checked: [],
    };
    const tableReducer = (state = defaultstate, action) => {
        switch (action.type) {
            case 'check':
                return Object.assign(Object.assign({}, state), { checked: action.data });
            default:
                return state;
        }
    };

    const store = redux.createStore(tableReducer);

    const { Text } = antd.Typography;
    const Index$1 = ({ checked = [], overdue, link, }) => {
        return (React__default["default"].createElement(antd.Space, null,
            React__default["default"].createElement(Text, { type: "success" },
                " \u5DF2\u52FE\u9009\u8BBE\u5907 ",
                checked.join('、')),
            React__default["default"].createElement(antd.Button, { onClick: () => overdue(checked), type: "default", danger: true }, "\u8FC7\u671F"),
            React__default["default"].createElement(antd.Button, { onClick: () => link(checked), type: "primary" }, "\u91CD\u8FDE")));
    };
    var Actions = reactRedux.connect(({ checked }) => {
        return {
            checked: checked,
        };
    })(Index$1);

    const columns = [
        {
            title: 'id',
            dataIndex: 'id',
        },
        {
            title: '名称',
            dataIndex: 'name',
        },
    ];
    const Index = ({ data, onSelect }) => {
        const selectTable = (ids) => {
            onSelect(ids);
        };
        return (React__default["default"].createElement(antd.Table, { rowKey: "id", pagination: false, dataSource: data, columns: columns, rowSelection: { onChange: selectTable } }));
    };
    var Table = reactRedux.connect(null, (dispatch) => {
        return {
            onSelect: (ids) => {
                dispatch({
                    type: 'check',
                    data: ids,
                });
            },
        };
    })(Index);

    var index = ({ id, data, overdue, link }) => {
        return (React__default["default"].createElement(reactRedux.Provider, { store: store },
            React__default["default"].createElement(antd.Space, { style: { width: '100%' }, direction: "vertical" },
                React__default["default"].createElement(antd.Row, { justify: "space-between" },
                    React__default["default"].createElement(antd.Col, null,
                        React__default["default"].createElement("p", null,
                            " \u8BBE\u5907\u5757 ",
                            id,
                            " \u5F53\u524D\u8FDE\u63A5\u677F ")),
                    React__default["default"].createElement(antd.Col, null,
                        React__default["default"].createElement(Actions, Object.assign({}, { overdue, link })))),
                React__default["default"].createElement(Table, { data: data }))));
    };

    exports.Device = index$1;
    exports.DeviceCountdown = index$2;
    exports.Link = index;
    exports.Upload = index$3;
    exports.Visitor = index$4;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
