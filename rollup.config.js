import commonjs from 'rollup-plugin-commonjs'
import typescript from 'rollup-plugin-typescript2'
import babel from 'rollup-plugin-babel'
import path from 'path'

import postcss from 'rollup-plugin-postcss'
import resolve from '@rollup/plugin-node-resolve'
import includePaths from 'rollup-plugin-includepaths'
// import { terser } from 'rollup-plugin-terser'
const getPath = (_path) => path.resolve(__dirname, _path)

import packageJSON from './package.json'

const externalAry = [
  'antd',
  'antd/es/locale/zh_CN',
  'antd/dist/antd.css',
  'moment',
  'moment/locale/zh-cn',
  'echarts',
  'prop-types',
  '@ant-design/icons',
  'react',
  'react-transition-group',
  'react-dnd',
  'react-dnd-html5-backend',
  'react-loadable',
  'react-resizable',
  'redux',
  'react-redux',
]

export default {
  input: getPath('./src/index.ts'),
  output: [
    {
      file: packageJSON.main,
      format: 'umd',
      name: 'ui_ly',
    },
    {
      file: packageJSON.module,
      format: 'es',
      name: 'ui_ly',
    },
  ],
  plugins: [
    typescript(), // 会自动读取 文件tsconfig.json配置
    resolve(),
    babel({ exclude: '**/node_modules/**', runtimeHelpers: true }),
    commonjs(),
    postcss({
      // Extract CSS to the same location where JS file is generated but with .css extension.
      extract: true,
      // Use named exports alongside default export.
      namedExports: true,
      // Minimize CSS, boolean or options for cssnano.
      minimize: true,
      // Enable sourceMap.
      sourceMap: true,
      // This plugin will process files ending with these extensions and the extensions supported by custom loaders.
      extensions: ['.less', '.css'],
    }),
    // terser(),
  ],
  //不能使用正则匹配，有定制化组件也是以echarts命名
  external: externalAry,
}
