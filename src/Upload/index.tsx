import React, { FC, memo } from 'react'
import { Form, Upload, Button } from 'antd'
import { InboxOutlined } from '@ant-design/icons'
import { upload } from './upload'

const normFile = (e: any) => {
  if (Array.isArray(e)) {
    return e
  }
  return e && e.fileList
}

export interface IUploadFile {
  maxSize?: number
  chunkSize?: number
  onCb: (data: any) => void
  url: string
}

const UploadFile: FC<IUploadFile> = (props) => {
  const [form] = Form.useForm()

  const { maxSize, onCb, chunkSize = 0.2, url } = props

  const SIZE = 1024 * 1024 * chunkSize // 每片大小

  const onUpload = () => {
    const { dragger } = form.getFieldsValue()

    const [{ originFileObj }] = dragger

    upload({
      file: originFileObj,
      chunkSize: SIZE,
      onCb,
      url,
    })
  }

  return (
    <Form name="control-ref">
      <Form.Item name="dragger" valuePropName="fileList" getValueFromEvent={normFile} noStyle>
        <Upload.Dragger name="files">
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-hint">上传文件不得超过 {maxSize || 100} MB</p>
        </Upload.Dragger>
      </Form.Item>
      <Form.Item style={{ marginTop: 20 }}>
        <Button type="primary" onClick={onUpload}>
          确定上传
        </Button>
      </Form.Item>
    </Form>
  )
}

export default memo(UploadFile)
