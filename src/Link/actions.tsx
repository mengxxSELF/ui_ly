import React from 'react'
import { Button, Space, Typography } from 'antd'
import { connect } from 'react-redux'

const { Text } = Typography

const Index = ({
  checked = [],
  overdue,
  link,
}: {
  checked: number[]
  overdue: (ids: number[]) => void
  link: (ids: number[]) => void
}) => {
  return (
    <Space>
      <Text type="success"> 已勾选设备 {checked.join('、')}</Text>
      <Button onClick={() => overdue(checked)} type="default" danger>
        过期
      </Button>
      <Button onClick={() => link(checked)} type="primary">
        重连
      </Button>
    </Space>
  )
}

export default connect(({ checked }: { checked: number[] }) => {
  return {
    checked: checked,
  }
})(Index)
