import React from 'react'
import { Space, Row, Col } from 'antd'
import { Provider } from 'react-redux'
import store from './store'

import Actions from './actions'
import Table from './table'

export interface ILink {
  id: number
  data: any[]
  overdue: (ids: number[]) => void
  link: (ids: number[]) => void
}

export default ({ id, data, overdue, link }: ILink) => {
  return (
    <Provider store={store}>
      <Space style={{ width: '100%' }} direction="vertical">
        <Row justify="space-between">
          <Col>
            <p> 设备块 {id} 当前连接板 </p>
          </Col>
          <Col>
            <Actions {...{ overdue, link }} />
          </Col>
        </Row>

        <Table data={data} />
      </Space>
    </Provider>
  )
}
