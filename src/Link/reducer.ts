const defaultstate = {
  checked: [],
}

const tableReducer = (state = defaultstate, action: any) => {
  switch (action.type) {
    case 'check':
      return { ...state, checked: action.data }
    default:
      return state
  }
}

export default tableReducer
