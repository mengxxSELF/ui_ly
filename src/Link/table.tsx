import React from 'react'
import { Table } from 'antd'
import { connect } from 'react-redux'

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
]

const Index = ({ data, onSelect }: { data: any[]; onSelect: any }) => {
  const selectTable = (ids: any[]) => {
    onSelect(ids)
  }

  return (
    <Table
      rowKey="id"
      pagination={false}
      dataSource={data}
      columns={columns}
      rowSelection={{ onChange: selectTable }}
    />
  )
}

export default connect(null, (dispatch) => {
  return {
    onSelect: (ids: number[]) => {
      dispatch({
        type: 'check',
        data: ids,
      })
    },
  }
})(Index)
