import React, { FC, memo } from 'react'

import { Card, Avatar, Tag, Space, Divider, Row, Col } from 'antd'

const { Meta } = Card

export interface IUserAttr {
  avatar: string
  name: string
  visitorCount: number
  isVip: boolean
  tags: string[]
  purpose: string[]
  description: string
}

export interface IUserCard {
  id: number
  user?: Partial<IUserAttr>
}

const UserCard: FC<IUserCard> = ({ id, ...defaultProps }) => {
  const { user } = defaultProps || { user: null }

  return (
    <Card title={`访客 ${id} 信息卡`} style={{ width: 300 }} hoverable={true} {...defaultProps}>
      <Space style={{ width: '100%' }} size={20} direction="vertical">
        {user ? (
          <Meta
            avatar={<Avatar src={user?.avatar} />}
            title={user?.name || '暂无用户名'}
            description={user?.description || '暂无描述信息'}
          />
        ) : null}
        <Row gutter={[10, 10]}>
          <Col span={10}>咨询次数</Col>
          <Col>{user?.visitorCount || 0}</Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col span={10}>VIP</Col>
          <Col>{user?.isVip ? '是' : '否'}</Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col span={10}>意向分析</Col>
          <Col>{user?.purpose?.join(',')}</Col>
        </Row>
      </Space>
      <Divider />
      <Space>{user?.tags && user.tags.map((item) => <Tag color="cyan">{item}</Tag>)}</Space>
    </Card>
  )
}

export default memo(UserCard)
