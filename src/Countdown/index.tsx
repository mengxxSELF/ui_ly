import React, { FC, memo } from 'react'

import { Card, Statistic, Button, Popconfirm } from 'antd'

const { Countdown } = Statistic

export interface ICountdown {
  id: number
  time: number
  cb: () => void
  [key: string]: any
}

const DeviceCountdown: FC<ICountdown> = (props) => {
  const { id, time, cb, ...defaultProps } = props

  return (
    <Card title={`设备 ${id} 有效期倒计时`}>
      <Countdown value={time} {...defaultProps} />
      <Popconfirm
        style={{ marginTop: 20 }}
        title={`确认对设备 ${id} 执行续签操作`}
        onConfirm={cb}
        okText="Yes"
        cancelText="No"
      >
        <Button> 续签 </Button>
      </Popconfirm>
    </Card>
  )
}

export default memo(DeviceCountdown)
