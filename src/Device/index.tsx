import { Modal, Button, Form, Input, Select } from 'antd'
import React, { useState } from 'react'

const { Option } = Select

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
}

interface IPart {
  id: number
  name: string
}

export default function ({ partData, onCb, ...params }: { partData: IPart[]; onCb: any }) {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [form] = Form.useForm()

  const handleOk = () => {
    const { name, part } = form.getFieldsValue()
    onCb({
      data: { name, part },
      cb: () => setIsModalVisible(false),
    })
  }

  const defaultModal = {
    width: 600,
    title: '创建子区设备块',
    ...params,
  }

  return (
    <>
      <Button type="primary" onClick={() => setIsModalVisible(true)}>
        创建子区设备块
      </Button>

      <Modal visible={isModalVisible} onOk={handleOk} onCancel={() => setIsModalVisible(false)} {...defaultModal}>
        <Form {...layout} form={form}>
          <Form.Item name="name" label="名称" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="part" label="可选区" rules={[{ required: true }]}>
            <Select allowClear>
              {partData.map((part) => {
                const { id, name } = part

                return (
                  <Option key={id} value={name}>
                    {name}
                  </Option>
                )
              })}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}
