# ui_ly

前端通用UI库

线上文档地址 [https://ui-ly-doc.vercel.app](https://ui-ly-doc.vercel.app/)

## Visitor 用户身份卡

```
  const defaultProps = {
    id: Math.round(Date.now() / 1000),
    user: {
      avatar: 'https://joeschmoe.io/api/v1/random',
      // description: 'description',
      tags: ['抖音', '播客'],
      purpose: [''],
    },
    // extra: <a href="#">More</a>,
  };

  <Visitor {...defaultProps} />
```

## Upload 切片上传

```
 const defaultProps = {
    maxSize: 50,
    chunkSize: 10,
    url: 'http://localhost:3011/batch/upload',
    onCb: (data: any) => console.log(data),

  };

  <Upload {...defaultProps} />

```

## DeviceCountdown 连接有效期倒计时

```

  <DeviceCountdown 
      {...{ 
        id: 1, 
        time: Date.now() + 10 * 1000,
        onFinish: () => console.log('onFinish')
      }} 
  />

```