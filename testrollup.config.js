import path from 'path'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import json from '@rollup/plugin-json'
import babel from 'rollup-plugin-babel'
import cModuleMap from './cModuleMap'

const extensions = ['.js', '.jsx']

// babel plugin
const babelPlugin = babel({
  exclude: 'node_modules/**', // 只编译源代码
  runtimeHelpers: true,
})

// 基础配置
const commonConf = {
  input: {
    index: './src/index.js',
    ...cModuleMap,
  },
  plugins: [resolve(extensions), babelPlugin, commonjs(), json()],
  external: ['react'],
}

const outputMap = {
  dir: 'lib', // 可以是 dir 表示输出目录 也可以是 file 表示输出文件
  format: 'es',
  sourceMap: true,
  entryFileNames: '[name]/index.js',
  exports: 'named',
}

export default {
  ...commonConf,
  outputMap,
}
